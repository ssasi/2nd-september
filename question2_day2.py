from day2_question1 import Node,LinkedList


def string_to_linkedlist(string1):

  linkedlist = LinkedList()
  for x in string1:
    if x in { "[","]", "(",")", "{","}" }:
      linkedlist.insertAtEnd(x)
  return linkedlist

def check_brackets(linkedlist):
  dict = {"[":"]", "(":")", "{":"}","]":"[",")":"(","}":"{"}
  if linkedlist.length % 2 == 0 and linkedlist.length !=0:
    while linkedlist.length > 0:
      linkedlist.print()
      start = linkedlist.deleteAtBeginning()
      end = linkedlist.deleteAtEnd()
      linkedlist.print()
      if dict[start] != end:
        print("no")
        return
    print("yes")
    return
  else:
    print("no")
    return
    
string1 = "lpel([{}])"

linkedlist = string_to_linkedlist(string1) 
check_brackets(linkedlist)

